# Videoprint

Make a histogram of video.

![cover](screenshot.jpg)

## Run it

    ./bootstrap # On Ubuntu
    ./videoprint.py in-file.mp4 out.png

## License

MIT
